# lua-inside-fpc

Examples that show how to run Lua code inside FPC/Lazarus programs - available for both CLI and GUI

Tutorial Article: [https://lazplanet.blogspot.com/2020/05/lua-code-inside-lazarus.html](https://lazplanet.blogspot.com/2020/05/lua-code-inside-lazarus.html)

If anything from Lua is contained in this project it is licensed under MIT License.

Other than that the project is free to use for any purpose.