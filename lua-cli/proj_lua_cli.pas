program proj_lua_cli;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes
  , lua53;

var
    L: Plua_State;
    result: integer;

begin
  L := luaL_newstate();
  luaL_openlibs(L);

  result := luaL_dostring(L, 'print ("I''m Lua, but I''m running inside".." Lazarus!!")');

  lua_close(L);
  ReadLn; // To keep the terminal window open
end.
